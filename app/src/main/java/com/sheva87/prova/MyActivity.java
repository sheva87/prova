package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MyActivity extends ActionBarActivity {
    private Context context=this;
    private TextView tGPS, tlocGPS;
    private TextView tNetwork, tlocNet;
    private Button salvami;
    private Button impostapos;
    private WebView myWebView;
    private LinearLayout layoutInd;
    private LinearLayout layoutGps;
    private LinearLayout layoutNet;
    private Intent mServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        myWebView = (WebView) findViewById(R.id.webview);
        tGPS = (TextView) findViewById(R.id.gps);
        tlocGPS = (TextView) findViewById(R.id.locgps);
        tNetwork = (TextView) findViewById(R.id.network);
        tlocNet = (TextView) findViewById(R.id.locnet);
        salvami = (Button) findViewById(R.id.salvami);
        impostapos = (Button) findViewById(R.id.impostapos);
        layoutInd = (LinearLayout) findViewById(R.id.layoutInd);
        layoutGps = (LinearLayout) findViewById(R.id.layoutgps);
        layoutNet = (LinearLayout) findViewById(R.id.layoutnet);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);

        //myWebView.loadData(s, "text/html", null);
        myWebView.loadUrl("file:///android_asset/osm3.html");

        //Toast.makeText(context, "NETWORK=" + location.getLatitude()+"|"+location.getLongitude(), Toast.LENGTH_SHORT).show();

        //Listener per i BOTTONI
        impostapos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                String s = b.getText().toString();
                if (s.equals("Imposta Posizione")) {
                    layoutGps.setVisibility(LinearLayout.GONE);
                    layoutNet.setVisibility(LinearLayout.GONE);
                    layoutInd.setVisibility(LinearLayout.VISIBLE);
                    b.setText("Annulla");
                    myWebView.loadUrl("javascript:setFlag(true)");
                } else if (s.equals("Annulla")) {
                    layoutGps.setVisibility(LinearLayout.VISIBLE);
                    layoutNet.setVisibility(LinearLayout.VISIBLE);
                    layoutInd.setVisibility(LinearLayout.GONE);
                    b.setText("Imposta Posizione");
                    myWebView.loadUrl("javascript:setFlag(false)");
                }
            }
        });

        salvami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.loadUrl("javascript:centra()");
            }
        });

        myWebView.setWebViewClient(new MyWebViewClient());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*
        //READ DB
        if (id == R.id.action1) {
            mServiceIntent = new Intent(this, DBService.class);
            mServiceIntent.putExtra("flag",0);
            this.startService(mServiceIntent);
            return true;
        }
        //WRITE DB
        if (id == R.id.action2) {
            mServiceIntent = new Intent(this, DBService.class);
            mServiceIntent.putExtra("flag",1);
            this.startService(mServiceIntent);
            return true;
        }   */
        if (id == R.id.setGeo) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public void localizzazione(){
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Log.i("INFO","GPSProviderEnabled");
            tGPS.setText("GPS ON!");
            myWebView.loadUrl("javascript:GPSOn()");
        }
        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            Log.i("INFO","NetworkProviderEnabled");
            tNetwork.setText("Network ON!");
            myWebView.loadUrl("javascript:NetworkOn()");
        }


        //CONTROLLO GEOLOCALIZZAZIONE:
        //Se entrambi i provider sono disabilitati allora apro un alertdialog per poter aprire le impostazioni
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.geoAlertText).setTitle(R.string.geoAlertTitle);
            // Add the buttons
            builder.setPositiveButton(R.string.ignora, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });

            // Create the AlertDialog
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        //listener GPS////////////////////////////////
        LocationListener llGPS = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                Log.i("INFO","LocationGPS:"+ location.getLatitude()+"|"+location.getLongitude());
                tlocGPS.setText("Location="+ location.getLatitude()+"|"+location.getLongitude());
                myWebView.loadUrl("javascript:displayGPSpos("+location.getLongitude()+", "+location.getLatitude()+")");
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                String s="";
                switch(status){
                    case LocationProvider.AVAILABLE :
                        s="AVAILABLE";
                        break;
                    case LocationProvider.OUT_OF_SERVICE:
                        s="OUT_OF_SERVICE";
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        s="TEMPORARILY_UNAVAILABLE";
                        break;
                }
                Log.i("INFO","GPSStatusChanged="+s);
                tGPS.setText("GPS ON! status="+s);
            }

            public void onProviderEnabled(String provider) {
                Log.i("INFO","GPSProviderEnabled");
                tGPS.setText("GPS ON!");
                myWebView.loadUrl("javascript:GPSOn()");
            }

            public void onProviderDisabled(String provider) {
                Log.i("INFO", "GPSProviderDisabled");
                tGPS.setText("GPS OFF");
                myWebView.loadUrl("javascript:GPSOff()");
            }
        };
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, llGPS);

        //listener Network////////////////////////////////
        LocationListener llNetwork = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                Log.i("INFO","LocationNetwork:"+ location.getLatitude()+"|"+location.getLongitude());
                tlocNet.setText("Location="+ location.getLatitude()+"|"+location.getLongitude());
                myWebView.loadUrl("javascript:displayNetpos("+location.getLongitude()+", "+location.getLatitude()+")");
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                String s="";
                switch(status){
                    case LocationProvider.AVAILABLE :
                        s="AVAILABLE";
                        break;
                    case LocationProvider.OUT_OF_SERVICE:
                        s="OUT_OF_SERVICE";
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        s="TEMPORARILY_UNAVAILABLE";
                        break;
                }
                Log.i("INFO","NetworkStatusChanged="+s);
                tNetwork.setText("Network ON! status="+s);
            }

            public void onProviderEnabled(String provider) {
                Log.i("INFO","NetworkProviderEnabled");
                tNetwork.setText("Network ON!");
                myWebView.loadUrl("javascript:NetworkOn()");
            }

            public void onProviderDisabled(String provider) {
                Log.i("INFO","NetworkProviderDisabled");
                tNetwork.setText("Network OFF");
                myWebView.loadUrl("javascript:NetworkOff()");
            }
        };
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, llNetwork);





    }

    //classe che serve per sapere quando la WebView � stata caricata completamente, per poi
    //richiamare altre funzioni dell'Activity
    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished (WebView view, String url){
            localizzazione();
        }

    }
}

