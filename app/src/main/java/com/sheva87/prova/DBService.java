package com.sheva87.prova;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Sheva87 on 06/05/2015.
 */
public class DBService extends IntentService {
    private FeedReaderDbHelper mDbHelper;
    private Context context;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public DBService() {
        super("DBService");
        mDbHelper = new FeedReaderDbHelper(this);
    }


    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
        //String dataString = workIntent.getDataString();

        // Do work here, based on the contents of dataString

        int flag=workIntent.getIntExtra("flag",0);
        switch(flag){
            case 0:
                int a=readdb();
                //Toast.makeText(this, "DB=" + a, Toast.LENGTH_LONG).show();
                Log.i("INFO", "DB=" + a);
                return;
            case 1:
                writedb(2,"prova2");
                return;
        }


    }



    public int readdb(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                FeedReaderContract.FeedEntry._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " DESC";

        Cursor c = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        c.moveToFirst();
        int s=c.getInt(0);
        return s;

    }

    public void writedb(int id, String title){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_ENTRY_ID, id);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE, title);
        //values.put(FeedEntry.COLUMN_NAME_CONTENT, content);

        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                FeedReaderContract.FeedEntry.TABLE_NAME,
                null,
                values);
    }
}
